var class_dont_use_1_1_logic_dummy =
[
    [ "GameSetupReady", "class_dont_use_1_1_logic_dummy.html#aead036d0a9d442eab971d678d0f99495", null ],
    [ "GameStateChanged", "class_dont_use_1_1_logic_dummy.html#a38c814d3ff9c18246dfbddd4fbd4342f", null ],
    [ "GetCurrentGameState", "class_dont_use_1_1_logic_dummy.html#a468e884eddb4430b30c65668c619a570", null ],
    [ "OnBeforePause", "class_dont_use_1_1_logic_dummy.html#ad586df07bd943b16ac9cba7ef4055d3b", null ],
    [ "OnBeforeRestart", "class_dont_use_1_1_logic_dummy.html#a80ec838f7dc742b67856c1fa9a31541a", null ],
    [ "OnFirstTimeSaveGameLoaded", "class_dont_use_1_1_logic_dummy.html#a6fb8354a0ca321be5a2b0dc6e0a5f871", null ],
    [ "OnSaveGameLoaded", "class_dont_use_1_1_logic_dummy.html#a1c60733cd38041a0d5c3593eff5f7bf4", null ],
    [ "Start", "class_dont_use_1_1_logic_dummy.html#a4bf0da771314253010d7907e7a86f48f", null ],
    [ "UpgradeSaveGameEntry", "class_dont_use_1_1_logic_dummy.html#ae2aacfeba78a75244bea7f9c5083233e", null ]
];