var hierarchy =
[
    [ "AssetPostprocessor", null, [
      [ "UGBLocaPostProcessor", "class_u_g_b_loca_post_processor.html", null ]
    ] ],
    [ "Attribute", null, [
      [ "UEDS.EditorSettingAttributeBase", "class_u_e_d_s_1_1_editor_setting_attribute_base.html", [
        [ "UEDS.EditorSettingAttribute", "class_u_e_d_s_1_1_editor_setting_attribute.html", null ],
        [ "UEDS.EditorSettingsContainerAttribute", "class_u_e_d_s_1_1_editor_settings_container_attribute.html", null ]
      ] ],
      [ "UEDS.EditorSettingIOProviderAttribute", "class_u_e_d_s_1_1_editor_setting_i_o_provider_attribute.html", null ]
    ] ],
    [ "UGBLocaParser.CLocaEntry", "class_u_g_b_loca_parser_1_1_c_loca_entry.html", null ],
    [ "DebugEntry", "class_debug_entry.html", null ],
    [ "Editor", null, [
      [ "CTransformSequenceInspector", "class_c_transform_sequence_inspector.html", null ]
    ] ],
    [ "EditorWindow", null, [
      [ "UEDSWindow", "class_u_e_d_s_window.html", null ]
    ] ],
    [ "EWSettingsBase", "class_e_w_settings_base.html", null ],
    [ "UEDS.ExistCheckParams", "class_u_e_d_s_1_1_exist_check_params.html", null ],
    [ "FieldRendererBase", "class_field_renderer_base.html", [
      [ "ColorFieldRenderer", "class_color_field_renderer.html", null ],
      [ "FloatFieldRenderer", "class_float_field_renderer.html", null ],
      [ "IntFieldRenderer", "class_int_field_renderer.html", null ],
      [ "Vector3FieldRenderer", "class_vector3_field_renderer.html", null ]
    ] ],
    [ "GameEvent", "class_game_event.html", null ],
    [ "GameEventManager", "class_game_event_manager.html", null ],
    [ "GameLogicImplementationBase", "class_game_logic_implementation_base.html", [
      [ "DontUse.LogicDummy", "class_dont_use_1_1_logic_dummy.html", null ]
    ] ],
    [ "GenericStateMachine", "class_generic_state_machine.html", null ],
    [ "Helpers", "class_helpers.html", null ],
    [ "IDisposable", null, [
      [ "Logger", "class_logger.html", null ]
    ] ],
    [ "UEDS.IGlobalEditorSettingsIOProvider", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html", [
      [ "UEDS.DefaultIOProvider", "class_u_e_d_s_1_1_default_i_o_provider.html", null ]
    ] ],
    [ "ILoadingScreenController", "interface_i_loading_screen_controller.html", [
      [ "CLoadingSceneController", "class_c_loading_scene_controller.html", null ]
    ] ],
    [ "ILogFormatter", "interface_i_log_formatter.html", null ],
    [ "ILogWriter", "interface_i_log_writer.html", null ],
    [ "InputDelegates", "class_input_delegates.html", null ],
    [ "IPlayerController", "interface_i_player_controller.html", null ],
    [ "UEDS.IPrimitveSerializer", "interface_u_e_d_s_1_1_i_primitve_serializer.html", [
      [ "UEDS.ColorSerializer", "class_u_e_d_s_1_1_color_serializer.html", null ],
      [ "UEDS.Vector3Serializer", "class_u_e_d_s_1_1_vector3_serializer.html", null ]
    ] ],
    [ "KeyMapping", "class_key_mapping.html", null ],
    [ "LESceneMenu", "class_l_e_scene_menu.html", null ],
    [ "LESceneMenuCommand", "class_l_e_scene_menu_command.html", [
      [ "LEDeleteSelectedCommand", "class_l_e_delete_selected_command.html", null ],
      [ "LESaveSceneCommand", "class_l_e_save_scene_command.html", null ]
    ] ],
    [ "LicenseInformation", "class_license_information.html", null ],
    [ "LocaData", "class_loca_data.html", null ],
    [ "LString", "class_l_string.html", null ],
    [ "MiniJSON", "class_mini_j_s_o_n.html", null ],
    [ "MonoBehaviour", null, [
      [ "CDebugState", "class_c_debug_state.html", null ],
      [ "CLoadingScene", "class_c_loading_scene.html", null ],
      [ "CLoadingSceneController", "class_c_loading_scene_controller.html", null ],
      [ "CLogCatcher", "class_c_log_catcher.html", null ],
      [ "CTransformSequence", "class_c_transform_sequence.html", null ],
      [ "CUGBObjectPoolEntry", "class_c_u_g_b_object_pool_entry.html", null ],
      [ "FPSMeter", "class_f_p_s_meter.html", null ],
      [ "Game", "class_game.html", null ],
      [ "GameComponent", "class_game_component.html", [
        [ "CPinchGesture", "class_c_pinch_gesture.html", null ],
        [ "GameData", "class_game_data.html", null ],
        [ "GameLicense", "class_game_license.html", null ],
        [ "GameLocalization", "class_game_localization.html", null ],
        [ "GameMusic", "class_game_music.html", null ],
        [ "GameOptions", "class_game_options.html", null ],
        [ "GamePause", "class_game_pause.html", null ],
        [ "GamePlayer", "class_game_player.html", null ],
        [ "GameSaveGames", "class_game_save_games.html", null ],
        [ "GameStateManager", "class_game_state_manager.html", null ],
        [ "LocalizableText", "class_localizable_text.html", null ],
        [ "SceneTransition", "class_scene_transition.html", null ],
        [ "TouchDetection", "class_touch_detection.html", [
          [ "GameInput", "class_game_input.html", null ]
        ] ]
      ] ],
      [ "LazyUpdates", "class_lazy_updates.html", null ],
      [ "MakePrefab", "class_make_prefab.html", null ],
      [ "TestObjectPool", "class_test_object_pool.html", null ],
      [ "ThreadingBridge", "class_threading_bridge.html", null ],
      [ "XLog", "class_x_log.html", null ]
    ] ],
    [ "PinchGestureEvent", "class_pinch_gesture_event.html", null ],
    [ "Serialization.SaveGameData", "class_serialization_1_1_save_game_data.html", null ],
    [ "Serialization.SaveGameEntry", "class_serialization_1_1_save_game_entry.html", null ],
    [ "UEDS.SerializedPrimitive", "class_u_e_d_s_1_1_serialized_primitive.html", null ],
    [ "UEDS.SerializedRoot", "class_u_e_d_s_1_1_serialized_root.html", null ],
    [ "UEDS.SerializedSettingsContainer", "class_u_e_d_s_1_1_serialized_settings_container.html", null ],
    [ "UEDS.SerializedValue", "class_u_e_d_s_1_1_serialized_value.html", null ],
    [ "UEDS.Setting", "class_u_e_d_s_1_1_setting.html", null ],
    [ "UEDS.SettingsContainer", "class_u_e_d_s_1_1_settings_container.html", null ],
    [ "UEDS.SettingsProxy", "class_u_e_d_s_1_1_settings_proxy.html", null ],
    [ "SGameEventType", "struct_s_game_event_type.html", null ],
    [ "SGameState", "struct_s_game_state.html", null ],
    [ "SLanguages", "struct_s_languages.html", null ],
    [ "SMusicState", "class_s_music_state.html", null ],
    [ "SPlayerState", "struct_s_player_state.html", null ],
    [ "StopWatch", "class_stop_watch.html", null ],
    [ "IOBridge.Storage", "class_i_o_bridge_1_1_storage.html", null ],
    [ "Helper.ThreadingHelper", "class_helper_1_1_threading_helper.html", null ],
    [ "TouchInformation", "class_touch_information.html", null ],
    [ "UGBLocaParser", "class_u_g_b_loca_parser.html", null ],
    [ "UGBObjectPool", "class_u_g_b_object_pool.html", null ],
    [ "UIHelpers", "class_u_i_helpers.html", null ],
    [ "UnityGameBaseVersionMenu", "class_unity_game_base_version_menu.html", null ],
    [ "UEDS.Utils", "class_u_e_d_s_1_1_utils.html", null ],
    [ "IOBridge.WrappedIO", "class_i_o_bridge_1_1_wrapped_i_o.html", null ],
    [ "XmlLocaData", "class_xml_loca_data.html", null ],
    [ "XmlLocaData.XmlLocaDataEntry", "class_xml_loca_data_1_1_xml_loca_data_entry.html", null ],
    [ "Serialization.SaveGameData.XmlSaveGame", "class_serialization_1_1_save_game_data_1_1_xml_save_game.html", null ],
    [ "Serialization.SaveGameData.XmlSaveGameEntry", "class_serialization_1_1_save_game_data_1_1_xml_save_game_entry.html", null ]
];