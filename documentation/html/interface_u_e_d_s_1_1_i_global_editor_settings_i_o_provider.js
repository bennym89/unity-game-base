var interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider =
[
    [ "CreateEmpty< T >", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html#ab56c197fcd04ba099afe4bad00443a13", null ],
    [ "Exists", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html#a0f00f6c1889950ed14269f32882deee2", null ],
    [ "GetData< T >", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html#ad8fd697b2bcafead0f8e9e0d61616923", null ],
    [ "Load< T >", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html#acf3a2d3899a2e3c8b5c56de801ef5399", null ],
    [ "Save< T >", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html#a2ef491c2ea08e37d72dd503d1e81eab8", null ],
    [ "LoaderError", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html#a2811d6eb7691a83b98fbe631caac91ef", null ],
    [ "LoaderFinished", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html#a0d1664eedff229b2eb347f9a58acb72a", null ],
    [ "LoaderHasError", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html#ae01623fa548c668e83781490e0922005", null ],
    [ "WriterError", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html#a0b9ef3060afe13fe9089d6d6a605891e", null ],
    [ "WriterFinished", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html#ab384cf8de8f802265a7f27c06085ef05", null ],
    [ "WriterHasError", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html#a603cd49f757bbaffe28b0bcf67f4d42c", null ]
];