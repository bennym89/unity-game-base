var class_stop_watch =
[
    [ "StopWatch", "class_stop_watch.html#a86f6bd6c54e709beccb96f9ead88b1b8", null ],
    [ "Measure", "class_stop_watch.html#ae6167f58aedf38d85e3c2e8ede0f1998", null ],
    [ "Pause", "class_stop_watch.html#a8e657d2d045964bea0a7446e1914e117", null ],
    [ "Start", "class_stop_watch.html#a7321c62736a806eab0c0afcb44cd8a0b", null ],
    [ "Stop", "class_stop_watch.html#af0454a94e1ecfe683db8bca8b5ce4ece", null ],
    [ "isRunning", "class_stop_watch.html#af4c117b0fbf339e82f78ee38a0157e23", null ]
];